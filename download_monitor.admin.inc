<?php

/**
 * @file
 * Administrative page callbacks for the download_monitor module.
 */
function download_monitor_admin_settings_form(&$form_state) {

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE
  );

  $form['settings']['download_limit'] = array(
    '#type' => 'textfield',
    '#size' => 1,
    '#title' => t('Allowed file downloads per user'),
    '#default_value' => variable_get('download_monitor_limit', 20),
    '#required' => TRUE
  );

  $form['settings']['timeframe'] = array(
    '#type' => 'select',
    '#title' => t('Within'),
    '#options' => variable_get('download_monitor_timeframe_options', array()),
    '#default_value' => variable_get('download_monitor_timeframe', 3600),
    '#required' => TRUE
  );

  $form['notifications'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification settings'),
    '#collapsible' => FALSE
  );

  $form['notifications']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Message subject'),
    '#description' => t('Message subject for notification email.'),
    '#default_value' => variable_get('download_monitor_message_subject',  t('Download monitor report on \'!site\'', array('!site' => variable_get('site_name', 'your Drupal site'))))
  );

  $form['notifications']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message body'),
    '#description' => t('Message body for notification email.'),
    '#default_value' => variable_get('download_monitor_message_body', t('')),
    '#rows' => 8
  );

  $timeframe_options = variable_get('download_monitor_timeframe_options', array());
  $header = array(t('Token'), t('Description'));
  $rows = array(
    array('!site', t('Your site name, current value: "') . variable_get('site_name', 'your Drupal site') . '"'),
    array('!limit', t('Allowed file download per user, current value: ') . variable_get('download_monitor_limit', 20)),
    array('!timeframe', t('Given timeframe, current value: ') . $timeframe_options[variable_get('download_monitor_timeframe', 20)]),
    array('!users', t('List of users who have exceeded the download limit, format: "&lt;username&gt; -- &lt;num&gt; files."')),
    array('!url', t('URL to report page, current value: ') . variable_get('download_monitor_url', ''))
  );

  $form['notifications']['replacement_patterns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => theme_table($header, $rows)
  );

  $form['notifications']['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Recipients'),
    '#description' => t('Enter comma separated list of recipients, e.g. john.doe@foobar.com, another.user@domain.tld.'),
    '#default_value' => variable_get('download_monitor_recipients', '')
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
  );

  return $form;
}

function download_monitor_admin_settings_form_validate($form, &$form_state) {
  // Validate file download limit value
  if( !isset($form['#post']['download_limit']) ||
      !is_numeric($form['#post']['download_limit']) ||
      $form['#post']['download_limit'] < 1 ) {
    form_set_error('download_monitor_invalid_download_limit', t('Download limit is invalid.'));
  }

  // Validate recipients
  $recipients = explode(",", $form['#post']['emails']);

  foreach( $recipients as $recipient ) {
    $recipient = trim($recipient);
    if( strlen($recipient) > 0 && !preg_match("/(.*)@(.*)\.(.*)/", $recipient) )
      form_set_error('download_monitor_invalid_recipients', t('Invalid email addresses: ' . $recipient));
  }
}

function download_monitor_admin_settings_form_submit($form, &$form_state) {
  variable_set("download_monitor_limit", $form['#post']['download_limit']);
  variable_set("download_monitor_timeframe", $form['#post']['timeframe']);
  variable_set("download_monitor_recipients", $form['#post']['emails']);
  variable_set("download_monitor_message_subject", $form['#post']['subject']);
  variable_set("download_monitor_message_body", $form['#post']['body']);
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Menu callback for general reporting.
 */
function download_monitor_admin_report_page() {

  $header = array(
    array('data' => t('Username'), 'field' => 'username'),
    array('data' => t('Filename'), 'field' => 'filename'),
    array('data' => t('File description'), 'field' => 'description'),
    array('data' => t('IP Address'), 'field' => 'ip'),
    array('data' => t('Time'), 'field' => 'timestamp', 'sort' => 'desc')
  );

  $result = pager_query("SELECT download_monitor.username, files.filepath, upload.description, download_monitor.ip, download_monitor.timestamp
                         FROM download_monitor, files, upload, node
                         WHERE download_monitor.fid = files.fid AND 
                         files.fid = upload.fid AND
                         node.vid = upload.vid AND
                         node.nid = upload.nid" . tablesort_sql($header), $limit = 20);

  $data = array();

  while( $row = db_fetch_object($result) ) {
    $user = user_load(array("name" => $row->username));
    $regexp_array = array();
    preg_match("/sites\/([^\/]*)\/files\/(.*)/", $row->filepath, $regexp_array);
    $data[] = array(l($user->name, "user/" . $user->uid), l($regexp_array[2], "system/files/" . $regexp_array[2]), $row->description, $row->ip, _download_monitor_format_period_difference($row->timestamp));
  }

  $html .= theme('table', $header, $data);
  $html .= theme('pager');

  if( count($data) == 0 )
    return "<p>" . t('No files have been downloaded yet, check back later.') . "</p>";
  else
    return $html;
}

/**
 * Menu callback for general reporting.
 */
function download_monitor_admin_report_page_by_user() {

  $timeframe_options = variable_get('download_monitor_timeframe_options', array());
  $timeframe = variable_get('download_monitor_timeframe', 3600);

  $header = array(
    array('data' => t('Username'), 'field' => 'username'),
    array('data' => t('Unique file downloads within the last %timeframe', array('%timeframe' => $timeframe_options[$timeframe])), 'field' => 'count'),
  );

  $result = db_query("SELECT username, COUNT(DISTINCT fid) AS count
                      FROM {download_monitor}
                      WHERE timestamp BETWEEN (UNIX_TIMESTAMP()-$timeframe) AND UNIX_TIMESTAMP()
                      GROUP BY username");

  $data = array();

  while( $row = db_fetch_object($result) ) {
  $user = user_load(array("name" => $row->username));
    $data[] = array(l($user->name, "user/" . $user->uid), $row->count);
  }

  $html .= theme('table', $header, $data);
  $html .= theme('pager');

  if( count($data) == 0 )
    return t('<p>No files have been downloaded within the last %timeframe, check back later.</p>', array('%timeframe' => $timeframe_options[$timeframe]));
  else
    return $html;
}

/**
 * Taken from http://www.konordo.com/blog/drupal-time-ago-function-templatephp
 */
function _download_monitor_format_period_difference($timestamp){
  $difference = time() - $timestamp;
  $periods = array(t("second"), t("minute"), t("hour"), t("day"), t("week"), t("month"), t("years"), t("decade"));
  $lengths = array("60","60","24","7","4.35","12","10");
  for($j = 0; $difference >= $lengths[$j]; $j++){
    $difference /= $lengths[$j];
  }
  $difference = round($difference);
  $text = format_plural($difference, "1 @period ago", "@count @periods ago", array('@period' => $periods[$j]));
  return $text;
}
