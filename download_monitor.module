<?php

/**
 * Implementation of hook_perm().
 */
function download_monitor_perm() {
  return array('administer download monitor', 'access download monitor stats');
}

/**
 * Implementation of hook_menu().
 */
function download_monitor_menu() {

  $items['admin/settings/download-monitor'] = array(
    'title' => t('Download monitor'),
    'description' => t('Set file download limits within a given timeframe and configure notification email addresses.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('download_monitor_admin_settings_form'),
    'access arguments' => array('administer download monitor'),
    'file' => 'download_monitor.admin.inc',
  );

  $items['admin/reports/download-monitor'] = array(
    'title' => t('Download monitor'),
    'description' => t('List all downloaded files.'),
    'page callback' => 'download_monitor_admin_report_page',
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('access download monitor stats'),
    'file' => 'download_monitor.admin.inc',
  );

  $items['admin/reports/download-monitor/all'] = array(
    'title' => t('All downloads'),
    'description' => t('List all downloaded files.'),
    'page callback' => 'download_monitor_admin_report_page',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access arguments' => array('access download monitor stats'),
    'file' => 'download_monitor.admin.inc',
  );

  $items['admin/reports/download-monitor/by-user'] = array(
    'title' => t('Download counts by user'),
    'description' => t('List how many files users have downloaded within the defined period.'),
    'page callback' => 'download_monitor_admin_report_page_by_user',
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('access download monitor stats'),
    'file' => 'download_monitor.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_file_download().
 */
function download_monitor_file_download($filepath) {
  // Make sure we are really requesting the file,
  // path must start with "system/files"
  $pos = strpos($_GET['q'], "system/files/");

  if( $pos !== false && $pos == 0 ) {
    // Find the file id and save the data
    $result = db_query("SELECT files.fid FROM files, upload, node WHERE files.fid = upload.fid AND upload.vid = node.vid AND upload.nid = node.nid AND files.filepath LIKE '%/%s'", $filepath);
    if( $result && $result->num_rows == 1 ) {
      $row = db_fetch_object($result);
      $fid = $row->fid;
      global $user;
      $username = $user->name;
      $ip = $_SERVER['REMOTE_ADDR'];
      db_query("INSERT INTO {download_monitor} (username, ip, fid, timestamp) VALUES ('$username', '$ip', '$fid', UNIX_TIMESTAMP())");
    }
  }
}

/**
 * Implementation of hook_cron().
 */
function download_monitor_cron() {
  // Check if some users have exceeded the download limit and send an email to the named recipients.

  $timeframe_options = variable_get('download_monitor_timeframe_options', array());
  $timeframe = variable_get('download_monitor_timeframe', 3600);

  // If we are running cron more often than our timeframe period, allow only one execution during that time
  if( variable_get('download_monitor_last_run', time()) > (time() - $timeframe) )
    return;

  $result = db_query("SELECT username, COUNT(DISTINCT fid) AS count
                      FROM {download_monitor}
                      WHERE timestamp BETWEEN (UNIX_TIMESTAMP()-$timeframe) AND UNIX_TIMESTAMP()
                      GROUP BY username");

  $limit = variable_get('download_monitor_limit', 20);
  $exceeded = array();

  // Check if someone has exceeded the download limit
  if( $result && $result->num_rows > 0 ) {
    while( $row = db_fetch_object($result) ) {
      if( $row->count > $limit ) {
        $exceeded[] = array("username" => $row->username, "count" => $row->count);			
      }
    }
  }

  // Exit if we have nothing to do
  if( count($exceeded) == 0 )
    return;

  $exceeded_text = "";
  foreach( $exceeded as $row )
    $exceeded_text .= $row['username'] . " -- " . $row['count'] . " files." . "\n";

  $timeframe_options = variable_get('download_monitor_timeframe_options', array());

  global $base_url;
  if( variable_get('clean_url', 0) )
    $path = "/admin/reports/download-monitor";
  else
    $path = "/?q=admin/reports/download-monitor";

  $recipients = explode(",", variable_get('download_monitor_recipients', array()));
  foreach( $recipients as $recipient ) {
    $message = array(
      'to' => $recipient,
      'subject' => t(variable_get('download_monitor_message_subject', t('Download monitor report on your Drupal site')),
                     array('!site' => variable_get('site_name', t('your Drupal site')))),
      'body' => t(variable_get('download_monitor_message_body', ''),
                  array('!site' => variable_get('site_name', 'your Drupal site'),
                        '!limit' => variable_get('download_monitor_limit', 20),
                        '!timeframe' => $timeframe_options[variable_get('download_monitor_timeframe', 3600)],
                        '!users' => $exceeded_text,
                        '!url' => $base_url . $path)
                ),
      'headers' => array('From' => variable_get('site_mail', 'no-reply@domain.tld'))
    );

    if( !drupal_mail_send($message) )
      watchdog("Download monitor", "Failed to send a notification email to " . $recipient);
  }

  // Set a variable when the check was last run
  variable_set('download_monitor_last_run', time());
}
